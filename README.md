# Wheream
[![Build Status](https://travis-ci.org/ali322/Wheream.svg?branch=master)](https://travis-ci.org/ali322/Wheream)
[![Dependency Status](https://gemnasium.com/badges/github.com/ali322/Wheream.svg)](https://gemnasium.com/github.com/ali322/Wheream)
[![Code Climate](https://codeclimate.com/github/ali322/Wheream/badges/gpa.svg)](https://codeclimate.com/github/ali322/Wheream)

> LBS App build with React-Native

## Screenshots
![Wheream1](http://7xvyds.com1.z0.glb.clouddn.com/2016-07-28_EBF2C6390E3CBFEAA7D3155611519AFC.jpg?imageView2/0/w/320/h/568)

## Develop
1. clone this repo
`git clone https://github.com/ali322/Wheream`
2. install all the packages
`npm install`
3. run the app in simulator on your own
`react-native run-ios` for ios or `react-native run-android` for android

## License

[MIT License](http://en.wikipedia.org/wiki/MIT_License)

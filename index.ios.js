'use strict'

import {AppRegistry} from "react-native"
import App from "./app/bootstrap"

AppRegistry.registerComponent('Wheream', () => App)

'use strict'

const apiHost = "http://apis.baidu.com"
const mapAPIHost = "http://api.map.baidu.com"
const cloudAPIHost = "https://d.apicloud.com"

export default {
    location:`${mapAPIHost}/geocoder/v2/`,
    search:`${mapAPIHost}/place/v2/suggestion`,
    surround:`${mapAPIHost}/place/v2/search`,
    weather:`${apiHost}/heweather/weather/free`,
    register:`${cloudAPIHost}/mcm/api/user`,
    login:`${cloudAPIHost}/mcm/api/user/login`,
    logout:`${cloudAPIHost}/mcm/api/user/logout`,
    user:`${cloudAPIHost}/mcm/api/user`
}
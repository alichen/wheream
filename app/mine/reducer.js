'use strict'

import * as constants from "./constant"

export function userReducer(state={
    user:{}
},action){
    switch(action.type){
        case constants.CHANGE_FIELD:
            return {
                ...state,
                user:{
                    ...state.user,
                    ...action.user
                }
            }
        case constants.REQUEST_USER:
            return {
                ...state,
                userFetching:true
            }
        case constants.RESPONSE_USER:
            return {
                ...state,
                userFetching:false,
                userFetched:true,
                user:action.payload
            }
        case constants.START_LOGIN:
            return {
                ...state,
                userLogining:true
            }
        case constants.FINISH_LOGIN:
            return {
                ...state,
                userLogining:false,
                userLogined:true,
                loginResult:action.payload
            }
        case constants.START_REGISTER:
            return {
                ...state,
                userRegistering:true
            }
        case constants.FINISH_REGISTER:
            return {
                ...state,
                userRegistering:false,
                userRegistered:true,
                registerResult:action.payload
            }
        case constants.START_LOGOUT:
            return {
                ...state,
                userLogouting:true
            }
        case constants.FINISH_LOGOUT:
            return {
                ...state,
                userLogouting:false,
                userLoguted:true,
                logoutResult:action.payload
            }
        default:
            return state
    }
}
'use strict'

import React,{Component} from "react"
import {View,Text,TouchableOpacity,Image} from "react-native"
import Icon from "react-native-vector-icons/FontAwesome"
import defaultStyles from "./stylesheet/mine"
import preferredThemer from "../common/theme"
import NavBar from "../common/component/navbar"
import Toast from "../common/component/toast"
import Alert from "../common/component/alert"
import {formatTime} from "../lib/helper"

import containerByComponent from "../lib/redux-helper"
import {fetchUser,logout} from "./action"
import {userReducer} from "./reducer"

@preferredThemer(defaultStyles)
export class Mine extends Component{
    constructor(props){
        super(props)
        this._handleLogout = this._handleLogout.bind(this)
    }
    componentDidMount(){
        const {authentication,actions} = this.props
        if(authentication){
            actions.fetchUser({
                userId:authentication.id
            })
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.userLoguted && !this.props.userLoguted){
            this._toast.show("注销成功")
        }
    }
    _handleLogout(){
        this._alert.alert("确定要注销?","",[
            {text:"取消",style:"cancel"},
            {text:"确定",onPress:()=>{
                this.props.actions.logout()
                this.props.navigationActions.resetScene()
            }}
        ])
    }
    renderNavigationBar(){
        const {styles,actions} = this.props
        const rightButton = (
            <TouchableOpacity style={styles.navigationBarButton} onPress={this._handleLogout}>
                <Icon name="cog" size={20} color="#999"/>
            </TouchableOpacity>
        )
        return (
            <NavBar title="登录"  rightButton={rightButton} userPrefs={this.props.userPrefs}/>
        )
    }
    renderBreif() {
        const {user,styles} = this.props
        if (!user) {
            return null
        }
        return (
            <View style={styles.mineBreif}>
                <View style={styles.mineAuthorize}>
                    <Image source={{ uri: user.avatar_url }} style={styles.mineAvatar}/>
                    <Text style={styles.mineAuthorizeText}>{user.loginname}</Text>
                    <Text style={[styles.mineAuthorizeText, styles.mineAuthorizeSubtext]}>注册时间: <Text>{formatTime(user.create_at) }</Text></Text>
                </View>
            </View>
        )
    }
    render(){
        const {styles} = this.props
        return (
            <View style={styles.container}>
            {this.renderNavigationBar()}
            <View style={styles.mineWrapper}>
            {this.renderBreif()}
            </View>
            <Toast ref={view=>this._toast=view}/>
            <Alert ref={view=>this._alert=view}/>
            </View>
        )
    }
}

export default containerByComponent(Mine,userReducer,{fetchUser,logout})
'use strict'

import React from "react"
import {Scene} from "../common/navigation/router"
import Mine from "./mine"
import Login from "./login"
import Register from "./register"

export default [
    <Scene key="mine" name="mine" component={Mine}/>,
    <Scene key="login" name="login" hideTabBar={true} component={Login}/>,
    <Scene key="register" name="register" hideTabBar={true} component={Register}/>
]
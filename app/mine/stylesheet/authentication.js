'use strict'

import {StyleSheet,Platform,Dimensions} from "react-native"

const screenHeight = Dimensions.get("window").height
const screenWidth = Dimensions.get("window").width

const styleForAll = {
    container:{
        flex:1,
        backgroundColor:"#FFF"
    },
    loginFormWrapper:{
        flex:1,
        flexDirection:"row",
        alignItems:"flex-start",
        justifyContent:"center"
    },
    loginForm:{
        paddingTop:screenHeight * 0.2,
        width:screenWidth * 0.7
    },
    loginFormRow:{
        paddingVertical:6,
        paddingHorizontal:8
    },
    loginFormInput:{
        borderColor:"#DDD",
        borderWidth:0.5,
        borderRadius:3,
        height:35,
        fontSize:14,
        paddingHorizontal:6,
        color:"#666"
    },
    loginFormButtons:{
        paddingTop:12,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    loginFormButton:{
        flex:1,
        marginHorizontal:8,
        paddingHorizontal:6,
        paddingVertical:10,
        borderColor:"#DDD",
        borderWidth:0.5,
        borderRadius:5
    },
    loginFormButtonText:{
        textAlign:"center",
        fontSize:15
    }
}

const styleForIOS = {}
const styleForAndroid = {}

export default StyleSheet.create({
    ...styleForAll,
    ...(Platform.OS === "ios"?styleForIOS:{}),
    ...(Platform.OS === "android"?styleForAndroid:{})
})
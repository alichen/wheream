'use strict'

import {StyleSheet,Platform} from "react-native"

const styleForAll = {
    container:{
        flex:1
    },
    navigationBarButton:{
        marginRight:8,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
        width:50
    },
    mineBreif:{
        height:150,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center"
    },
    mineAuthorize:{
        paddingHorizontal:8,
        paddingVertical:10,
        flexDirection:"column",
        alignItems:"center",
        justifyContent:"center"
    },
    mineAuthorizeText:{
        fontSize:14,
        paddingVertical:4,
        color:"#333" 
    },
    mineAuthorizeSubtext:{
      fontSize:13,
      color:"#666"  
    },
    mineAvatar:{
      width:80,
      height:80,
      borderRadius:40  
    },
    mineWrapper:{
        flex:1
    }
}

const styleForAndroid = {}

const styleForIOS = {}

export default StyleSheet.create({
    ...styleForAll,
    ...(Platform.OS === "android"?styleForAndroid:{}),
    ...(Platform.OS === "ios"?styleForIOS:{})
})
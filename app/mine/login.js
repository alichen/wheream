'use strict'

import React,{Component} from "react"
import {View,Text,TextInput,TouchableOpacity} from "react-native"
import NavBar from "../common/component/navbar"

import defaultStyles from "./stylesheet/authentication"
import preferredThemer from "../common/theme"

import * as allActions from "./action"
import {userReducer} from "./reducer"
import containerByComponent from "../lib/redux-helper"

@preferredThemer(defaultStyles)
class Login extends Component{
    constructor(props){
        super(props)
        this._changeUser = this._changeUser.bind(this)
        this._doLogin = this._doLogin.bind(this)
    }
    _changeUser(user){
        this.props.actions.changeField(user)
    }
    _doLogin(){
        this.props.actions.login(this.props.user)
    }
    componentWillReceiveProps(nextProps){
        const {navigationActions} = this.props
        if(nextProps.userLogined && this.props.userLogining){
            this._toast.show("登录成功!",navigationActions.popScene)
            this.props.saveAuthentication({
                ...this.props.user,
                id:nextProps.loginResult.userId
            })
        }
    }
    renderForm(){
        const {styles,navigationActions} = this.props
        return (
            <View style={styles.loginForm}>
                <View style={styles.loginFormRow}><TextInput placeholder="请输入用户名" onChangeText={value=>{
                    this._changeUser({username:value})
                }} style={styles.loginFormInput}/></View>
                <View style={styles.loginFormRow}><TextInput placeholder="请输入密码" onChangeText={value=>{
                    this._changeUser({password:value})
                }} style={styles.loginFormInput}/></View>
                <View style={styles.loginFormButtons}>
                    <TouchableOpacity style={styles.loginFormButton} onPress={this._doLogin}><Text style={styles.loginFormButtonText}>登录</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.loginFormButton} onPress={()=>navigationActions.pushScene("register")}><Text style={styles.loginFormButtonText}>立即注册</Text></TouchableOpacity>
                </View>
            </View>
        )
    }
    render(){
        const {styles} = this.props
        const {popScene} = this.props.navigationActions
        return (
            <View style={styles.container}>
            <NavBar title="登录" onLeftButtonClick={popScene} userPrefs={this.props.userPrefs}/>
            <View style={styles.loginFormWrapper}>{this.renderForm()}</View>
            </View>
        )
    }
}

export default containerByComponent(Login,userReducer,allActions)
'use strict'

import React,{Component} from "react"
import {View,Text,TextInput,TouchableOpacity} from "react-native"
import NavBar from "../common/component/navbar"
import Toast from "../common/component/toast"

import * as allActions from "./action"
import {userReducer} from "./reducer"
import containerByComponent from "../lib/redux-helper"

import defaultStyles from "./stylesheet/authentication"
import preferredThemer from "../common/theme"

@preferredThemer(defaultStyles)
class Register extends Component{
    constructor(props){
        super(props)
        this._changeUser = this._changeUser.bind(this)
        this._doRegister = this._doRegister.bind(this)
    }
    _changeUser(user){
        this.props.actions.changeField(user)
    }
    _doRegister(){
        this.props.actions.register(this.props.user)
    }
    componentWillReceiveProps(nextProps){
        const {navigationActions} = this.props
        if(nextProps.userRegistered && this.props.userRegistering){
            this._toast.show("注册成功!",navigationActions.popScene)
            this.props.saveAuthentication({
                ...this.props.user,
                id:nextProps.registerResult.id
            })
        }
    }
    renderForm(){
        const {styles,navigationActions} = this.props
        return (
            <View style={styles.loginForm}>
                <View style={styles.loginFormRow}><TextInput placeholder="请输入用户名" onChangeText={(value)=>{
                    this._changeUser({username:value})
                }} style={styles.loginFormInput}/></View>
                <View style={styles.loginFormRow}><TextInput placeholder="请输入密码" onChangeText={value=>{
                    this._changeUser({password:value})
                }}style={styles.loginFormInput}/></View>
                <View style={styles.loginFormRow}><TextInput placeholder="重复一次密码" onChangeText={value=>{
                    this._changeUser({repeatPassword:value})
                }} style={styles.loginFormInput}/></View>
                <View style={styles.loginFormRow}><TextInput placeholder="电子邮箱" onChangeText={value=>{
                    this._changeUser({email:value})
                }} style={styles.loginFormInput}/></View>
                <View style={styles.loginFormButtons}>
                    <TouchableOpacity style={styles.loginFormButton} onPress={this._doRegister}><Text style={styles.loginFormButtonText}>提交</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.loginFormButton} onPress={()=>navigationActions.popScene()}><Text style={styles.loginFormButtonText}>已有账号</Text></TouchableOpacity>
                </View>
            </View>
        )
    }
    render(){
        const {styles} = this.props
        const {popScene} = this.props.navigationActions
        return (
            <View style={styles.container}>
                <NavBar title="注册" onLeftButtonClick={popScene} userPrefs={this.props.userPrefs}/>
                <View style={styles.loginFormWrapper}>{this.renderForm()}</View>
                <Toast ref={view=>this._toast=view}/>
            </View>
        )
    }
}

export default containerByComponent(Register,userReducer,allActions)
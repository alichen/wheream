'use strict'

import request from "../lib/request"
import * as constants from "./constant"
import api from "../lib/api"
import {appSignatureHeader} from "../lib/helper"

function requestUser(){
    return {
        type:constants.REQUEST_USER
    }
}

function responseUser(payload){
    return {
        type:constants.RESPONSE_USER,
        respondAt:Date.now(),
        payload
    }
}

export function fetchUser(param){
    return dispatch=>{
        dispatch(requestUser())
        request.get(api.user,param,{
            headers:{
                ...(appSignatureHeader()),
                'Content-Type': 'application/json'
            }
        }).then(ret=>{
            dispatch(responseUser(ret))
        })
    }
}

function startLogin(){
    return {
        type:constants.START_LOGIN
    }
}

function finishLogin(payload){
    return {
        type:constants.FINISH_LOGIN,
        payload,
        finishAt:Date.now()
    }
}

export function login(param){
    return dispatch=>{
        dispatch(startLogin())
        request.post(api.login,param,{
            headers:{
                ...(appSignatureHeader()),
                'Content-Type': 'application/json'
            }}).then(ret=>{
            dispatch(finishLogin(ret))
        })
    }
}

function startRegister(){
    return {
        type:constants.START_REGISTER
    }
}

function finishRegister(payload){
    return {
        type:constants.FINISH_REGISTER,
        payload,
        finishAt:Date.now()
    }
}

export function register(param){
    return dispatch=>{
        dispatch(startRegister())
        request.post(api.register,param,{
            headers:{
                ...(appSignatureHeader()),
                'Content-Type': 'application/json'
            }
        }).then(ret=>{
            dispatch(finishRegister(ret))
        })
    }
}

function startLogout(){
    return {
        type:constants.START_LOGOUT
    }
}

function finishLogout(payload){
    return {
        type:constants.FINISH_LOGOUT,
        payload,
        finishAt:Date.now()
    }
}

export function logout(param){
    return dispatch=>{
        dispatch(startLogout())
        request.post(api.logout,param).then(ret=>{
            dispatch(finishLogout(ret))
        })
    }
}

export function changeField(param){
    return {
        type:constants.CHANGE_FIELD,
        user:param
    }
}
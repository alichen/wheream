'use strict'

import React from "react"
import {Scene} from "../common/navigation/router" 
import Surround from "./surround"

export default [
    <Scene component={Surround} name="surround" key="surround"/>
]
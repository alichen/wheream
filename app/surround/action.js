'use strict'

import * as constants from "./constant"
import request from "../lib/request"
import api from "../lib/api"

function requestSurround(){
    return {
        type:constants.REQUEST_SURROUND
    }
}

function responseSurround(payload){
    return {
        type:constants.RESPONSE_SURROUND,
        payload,
        respondAt:Date.now()
    }
}

export function fetchSurround(param){
    param = {...param,output:"json",ak:"6Qed1yuX6s3VOGhjGWZ1tqwr"}
    return dispatch=>{
        dispatch(requestSurround())
        request.get(api.surround,param).then(ret=>{
            dispatch(responseSurround(ret))
        })
    }
}
'use strict'

import {Platform,StyleSheet} from "react-native"

const styleForAll = {
    container:{
        flex:1,
        backgroundColor:"#FFF"
    },
    searchResultRow:{
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"flex-start",
        paddingVertical:10,
        paddingHorizontal:12
    },
    searchResultRowText:{
        color:"#666",
        flex:1
    },
    searchResultArrowIcon:{
        width:20,
        textAlign:"center"
    },
    cellSeparator:{
        height:0.5,
        backgroundColor:"#DDD"
    }
}

const styleForAndroid = {}
const styleForIOS = {}

export default StyleSheet.create({
    ...styleForAll,
    ...(Platform.OS === "android"?styleForAndroid:{}),
    ...(Platform.OS === "ios"?styleForIOS:{})
})
'use strict'

import React,{Component} from "react"
import {View,Text,ListView,TouchableOpacity} from "react-native"
import Icon from "react-native-vector-icons/Ionicons"

import * as allActions from "./action"
import {surroundReducer} from "./reducer"
import containerByComponent from "../lib/redux-helper"
import SearchBar from "../common/module/searchbar"

import preferredThemer from "../common/theme"
import defaultStyles from "./stylesheet/surround"

@preferredThemer(defaultStyles)
class Surround extends Component{
    constructor(props){
        super(props)
        this.state = {
            keyword:"",
            dataSource:new ListView.DataSource({
                rowHasChanged:(r1,r2)=>r1 !== r2
            })
        }
        this._renderRow = this._renderRow.bind(this)
        this._handleSearch = this._handleSearch.bind(this)
    }
    componentWillReceiveProps(nextProps){
        if(!nextProps.surroundFetching && this.props.surroundFetching){
            this.setState({
                dataSource:this.state.dataSource.cloneWithRows(nextProps.surround)
            })
        }
    }
    _renderRow(row){
        const {styles} = this.props
        return (
            <TouchableOpacity onPress={()=>{
            }} style={styles.searchResultRow}>
            <Text style={styles.searchResultRowText}>{row.name}</Text>
            <Icon name="md-arrow-dropright" size={18} color="#666" style={styles.searchResultArrowIcon}/>
            </TouchableOpacity>
        )
    }
    _handleSearch(keyword){
        const {userPrefs,actions} = this.props
        this.setState({
            keyword
        },()=>{
            actions.fetchSurround({q:keyword,region:userPrefs.location.city.replace(/[市]/g,"")})
        })
    }
    renderSearchBar(){
        return (
            <SearchBar onSearch={this._handleSearch} userPrefs={this.props.userPrefs}/>
        )
    }
    render(){
        const {styles} = this.props
        return (
            <View style={styles.container}>
            {this.renderSearchBar()}
            <ListView dataSource={this.state.dataSource} renderRow={this._renderRow} enableEmptySections={true} 
                renderSeparator={(sectionId,rowId)=><View key={`${sectionId}-${rowId}`} style={styles["cellSeparator"]}/>}/>
            </View>
        )
    }
}

export default containerByComponent(Surround,surroundReducer,allActions)
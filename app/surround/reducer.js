'use strict'

import * as constants from "./constant"

export function surroundReducer(state={},action){
    switch(action.type){
        case constants.REQUEST_SURROUND:
            return {
                ...state,
                surroundFetching:true
            }
        case constants.RESPONSE_SURROUND:
            return {
                ...state,
                surroundFetching:false,
                surroundFetched:true,
                surround:action.payload.results
            }
        default:
            return state
    }
}
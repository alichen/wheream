'use strict'

import React,{Component} from "react"
import {View,Text,TouchableOpacity} from "react-native"
import test from "ava"
import {shallow} from "enzyme"
import sinon from "sinon"
import {Mine} from "../../mine/mine"
import  * as actions from "../../mine/action"
import {userReducer} from "../../mine/reducer"
import {configureStore} from "../../lib/redux-helper"

function setup(){
    const store = configureStore(userReducer)
    const state = store.getState()
    let props = {
        styles:{},
        styleConstants:{},
        userPrefs:{},
        actions,
        ...state
    }
    sinon.spy(Mine.prototype,"render")
    sinon.spy(Mine.prototype,"renderNavigationBar")
    let wrapper = shallow(<Mine {...props}/>)
    return {wrapper,store}
}

let wrapper,store
test.before(t=>{
    const setupResult = setup()
    wrapper = setupResult.wrapper
    store = setupResult.store
})

test("area should render once",t=>{
    t.is(Mine.prototype.render.callCount,1,"render error")
    // wrapper.setProps({
    //     renderRow
    // })
    t.true(wrapper.find("View").length > 1)
})

test.skip("topics searchActive should toggled",t=>{
    const el = wrapper.instance()
    el.toggleSearchActive()
    t.is(wrapper.state().searchBarActive,true)
})
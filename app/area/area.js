'use strict'

import React, {Component} from "react"
import {View,Text,ListView,TouchableOpacity} from "react-native"
import MapView from "react-native-maps"
import Icon from "react-native-vector-icons/Ionicons"

import SearchBar from "../common/module/searchbar"
import containerByComponent from "../lib/redux-helper"
import * as allActions from "./action"
import {areaReducer} from "./reducer"

import preferredThemer from "../common/theme"
import defaultStyles from "./stylesheet/area"

@preferredThemer(defaultStyles)
export class Area extends Component {
    constructor(props){
        super(props)
        this._handleSearch = this._handleSearch.bind(this)
        this._renderRow = this._renderRow.bind(this)
        this._toggleResultVisble = this._toggleResultVisble.bind(this)
        this.state = {
            keyword:"",
            resultVisble:false,
            resultDataSource:new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2
            })
        }
    }
    componentDidMount(){
        this.props.actions.fetchGeolocation()
    }
    _handleSearch(keyword){
        this.setState({
            keyword
        },()=>{
            this.props.actions.fetchPlace({q:keyword,region:this.props.location.city.replace(/[市]/g,"")})
        })
    }
    renderSearchBar(){
        return (
            <SearchBar onSearch={this._handleSearch} userPrefs={this.props.userPrefs}/>
        )
    }
    componentDidUpdate(prevProps,prevState){
        const {geolocation,location,actions,saveUserPrefs,userPrefs} = this.props
        if(!prevProps.geolocation && geolocation && !geolocation.failed){
            actions.fetchLocation({
                location:`${geolocation.latitude},${geolocation.longitude}`
            })
        }
        if(geolocation && prevProps.geolocation && 
        (prevProps.geolocation.latitude !== geolocation.latitude 
        || prevProps.geolocation.longitude !== geolocation.longitude)){
            actions.fetchLocation({
                location:`${geolocation.latitude},${geolocation.longitude}`
            })
        }
        if(!prevProps.location && location){
            saveUserPrefs({
                ...userPrefs,
                coords:{
                    latitude:geolocation.latitude,longitude:geolocation.longitude
                },
                location
            })
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.place && nextProps.place.length > 0 && this.props.placeFetching){
            this.setState({
                resultVisble:true,
                resultDataSource:this.state.resultDataSource.cloneWithRows(nextProps.place)
            })
        }
    }
    _renderRow(row){
        const {styles,actions,saveUserPrefs,userPrefs,location} = this.props
        return (
            <TouchableOpacity onPress={()=>{
                this._toggleResultVisble()
                saveUserPrefs({
                    ...userPrefs,
                    coords:{
                        longitude:row.location.lng,
                        latitude:row.location.lat 
                    },
                    location
                })
                actions.changeGeolocation({
                    longitude:row.location.lng,
                    latitude:row.location.lat
                })
            }} style={styles.searchResultRow}>
            <Text style={styles.searchResultRowText}>{row.name}</Text>
            <Icon name="md-arrow-dropright" size={18} color="#666" style={styles.searchResultArrowIcon}/>
            </TouchableOpacity>
        )
    }
    _toggleResultVisble(){
        this.setState({resultVisble:!this.state.resultVisble})
    }
    renderResult(){
        const {place,styles} = this.props
        if(!place || !this.state.resultVisble)return null
        return (
            <View style={styles.searchResult}>
                <View style={styles.searchResultCount}>
                    <Text style={styles.searchResultCountText}>共找到{place.length}个结果</Text>
                    <TouchableOpacity style={styles.closeResult} onPress={this._toggleResultVisble}><Icon name="md-close" size={20} color="#FFF"/></TouchableOpacity>
                </View>
                <ListView dataSource={this.state.resultDataSource} renderRow={this._renderRow} enableEmptySections={true} 
                renderSeparator={(sectionId,rowId)=><View key={`${sectionId}-${rowId}`} style={styles["cellSeparator"]}/>}/>
            </View>
        )
    }
    render() {
        const {styles,geolocation,location} = this.props
        if(!geolocation || geolocation.failed){
            return null
        }
        const coordinate = {latitude:geolocation.latitude,longitude:geolocation.longitude}
        const locationDesc = location?location.address:"im here"
        return (
            <View style={styles.container}>
            {this.renderSearchBar()}
            <MapView style={styles.map} region={{
                ...geolocation,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            }}>
            <MapView.Marker coordinate={coordinate} title={locationDesc}>
            </MapView.Marker>
            <MapView.Circle center={coordinate} radius={1000} strokeColor="rgba(0,0,0,.3)" fillColor="rgba(0,0,0,.3)"/>
            </MapView>
            {this.renderResult()}
            </View>
        )
    }
}

export default containerByComponent(Area,areaReducer,allActions)
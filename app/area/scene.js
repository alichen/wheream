'use strict'

import React from "react"
import Area from "./area"
import {Scene} from "../common/navigation/router"

export default [
    <Scene component={Area} key="area" name="area"/>
]
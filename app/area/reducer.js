'use strict'

import * as constants from "./constant"

export function areaReducer(state={},action) {
    switch (action.type) {
        case constants.REQUEST_GEOLOCATION:
            return {
                ...state,
                geolocationFetching:true
            }
        case constants.RESPONSE_GEOLOCATION:
            return {
                ...state,
                geolocationFetching:false,
                geolocationFetched:!action.payload.failed,
                geolocation:action.payload
            }
        case constants.CHANGE_GEOLOCATION:
            return {
                ...state,
                geolocationChanged:true,
                geolocation:action.coords
            }
        case constants.REQUEST_LOCATION:
            return {
                ...state,
                locationFetching:true
            }
        case constants.RESPONSE_LOCATION:
            return {
                ...state,
                locationFetching:false,
                locationFetched:true,
                location:{...action.payload.result.addressComponent,address:action.payload.result.formatted_address}
            }
        case constants.REQUEST_PLACE:
            return {
                ...state,
                placeFetching:true
            }
        case constants.RESPONSE_PLACE:
            return {
                ...state,
                placeFetching:false,
                placeFetched:true,
                place:action.payload.result
            }
        case constants.REQUEST_WEATHER:
            return {
                ...state,
                weatherFetching:true
            }
        case constants.RESPONSE_WEATHER:
            return {
                ...state,
                weatherFetching:false,
                weatherFetched:true,
                weather:action.payload["HeWeather data service 3.0"][0]
            }
        default:
            return state
    }
}
'use strict'

import {StyleSheet,Platform,Dimensions} from "react-native"

const screenWidth = Dimensions.get("window").width
const screenHeight = Dimensions.get("window").height

const styleForAll = {
    container:{
        flex:1
    },
    map:{
        width:screenWidth,
        height:screenHeight - 50
    },
    searchResult:{
        position:"absolute",
        bottom:50,
        left:0,
        backgroundColor:"#FAFAFA",
        width:screenWidth
    },
    closeResult:{
        width:30,
        justifyContent:"center"
    },
    searchResultCount:{
        height:40,
        paddingLeft:8,
        backgroundColor:"royalblue",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center"
    },
    searchResultCountText:{
        color:"#FFF",
        flex:1,
        textAlign:"center"
    },
    searchResultRow:{
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"flex-start",
        paddingVertical:10,
        paddingHorizontal:12
    },
    searchResultRowText:{
        color:"#666",
        flex:1
    },
    searchResultArrowIcon:{
        width:20,
        textAlign:"center"
    },
    cellSeparator:{
        height:0.5,
        backgroundColor:"#DDD"
    }
}

const styleForAndroid = {}
const styleForIOS = {}

export default StyleSheet.create({
    ...styleForAll,
    ...(Platform.OS === "android"?styleForAndroid:{}),
    ...(Platform.OS === "ios"?styleForIOS:{})
})


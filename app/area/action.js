'use strict'

import * as constants from "./constant"
import request from "../lib/request"
import api from "../lib/api"

function requestGeolocation(){
    return {
        type:constants.REQUEST_GEOLOCATION
    }
}

function responseGeolocation(payload){
    return {
        type:constants.RESPONSE_GEOLOCATION,
        respondAt:Date.now(),
        payload
    }
}

export function fetchGeolocation(){
    return dispatch=>{
        dispatch(requestGeolocation())
        navigator.geolocation.getCurrentPosition(ret=>{
            dispatch(responseGeolocation(ret.coords))
        },err=>{dispatch(responseGeolocation({failed:true}))},{
            enableHighAccuracy:true,timeout:20000,maximumAge:1000
        })
    }
}

export function changeGeolocation(coords){
    return {
        type:constants.CHANGE_GEOLOCATION,
        coords
    }
}

function requestLocation(){
    return {
        type:constants.REQUEST_LOCATION
    }
}

function responseLocation(payload){
    return {
        type:constants.RESPONSE_LOCATION,
        respondAt:Date.now(),
        payload
    }
}

export function fetchLocation(param){
    param = {...param,output:"json",ak:"6Qed1yuX6s3VOGhjGWZ1tqwr"}
    return dispatch=>{
        dispatch(requestLocation())
        request.get(api.location,param).then(ret=>{
            dispatch(responseLocation(ret))
        })
    }
}

function requestPlace(){
    return {
        type:constants.REQUEST_PLACE
    }
}

function responsePlace(payload){
    return {
        type:constants.RESPONSE_PLACE,
        respondAt:Date.now(),
        payload
    }
}

export function fetchPlace(param){
    param = {...param,output:"json",ak:"6Qed1yuX6s3VOGhjGWZ1tqwr"}
    return dispatch=>{
        dispatch(requestPlace())
        request.get(api.search,param).then(ret=>{
            dispatch(responsePlace(ret))
        })
    }
}

function requestWeather(){
    return {
        type:constants.REQUEST_WEATHER
    }
}

function responseWeather(payload){
    return {
        type:constants.RESPONSE_WEATHER,
        payload,
        respondAt:Date.now()
    }
}

export function fetchWeather(param){
    return dispatch=>{
        dispatch(requestWeather())
        request.get(api.weather,param,{headers:{apikey:"50f4cd1fb393788826059850f1932d01"}}).then(ret=>{
            dispatch(responseWeather(ret))
        })
    }
}
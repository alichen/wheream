'use strict'

import React,{Component} from "react"
import {View,Text,PropTypes,StyleSheet} from "react-native"
import {combineReducers,bindActionCreators} from "redux"
import containerByComponent from "./lib/redux-helper"
import {userPrefsReducer,authenticationReducer} from "./common/reducer"
import {fetchUserPrefs,saveUserPrefs,fetchAuthentication,saveAuthentication} from "./common/action"
import routerReducer from "./common/navigation/reducer"
import Router,{Scene} from "./common/navigation/router"
import Alert from "./common/component/alert"

import Storage from "./lib/storage"
global.storage = new Storage()

import areaScenes from "./area/scene"
import surroundScenes from "./surround/scene"
import mineScenes from "./mine/scene"

class App extends Component{
    constructor(props){
        super(props)
        this._handleSelect = this._handleSelect.bind(this)
    }
    _handleSelect(navState,navActions){
        if(!this.props.authentication){
            this._alert.alert("请先登录","登录",[
                {text:"取消",style:"cancel"},
                {text:"确定",onPress:()=>navActions.pushScene("login")}
            ])
            return false
        }
        return true
    }
    render(){
        const sceneProps = {
            saveUserPrefs:this.props.actions.saveUserPrefs,
            userPrefs:this.props.userPrefs,
            saveAuthentication:this.props.actions.saveAuthentication,
            authentication:this.props.authentication
        }
        return (
            <View style={styles.container}>
                <Router initialSceneKey="tabs" sceneProps={sceneProps}
                navigationState={this.props.navigationState} dispatch={this.props.dispatch}>
                    <Scene key="tabs" name="tabs" tabbar={true}>
                        <Scene key="tab_1" name="tab_1" title="位置" iconName="map">{areaScenes}</Scene>
                        <Scene key="tab_2" name="tab_2" title="周边" iconName="rocket">{surroundScenes}</Scene>
                        <Scene key="tab_3" name="tab_3" title="我的" iconName="user" onSelect={this._handleSelect}>{mineScenes}</Scene>
                    </Scene>
                </Router>
                <Alert ref={view=>this._alert=view}/>
            </View>    
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    }
})

const rootReducer = combineReducers({
    routerReducer,
    userPrefsReducer,
    authenticationReducer
})

export default containerByComponent(App,rootReducer,dispatch=>({
    dispatch,
    actions:bindActionCreators({fetchUserPrefs,saveUserPrefs,fetchAuthentication,saveAuthentication},dispatch)
}),null,state=>({
    ...state.userPrefsReducer,
    ...state.authenticationReducer,
    navigationState:state.routerReducer
}))
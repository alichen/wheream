'use strict'

import * as constants from "./constant"

export function userPrefsReducer(state={
    userPrefs:{
        preferredFontSize:14,
        preferredTheme:"light"
    }
},action){
    switch(action.type){
        case constants.REQUEST_USERPREFS:
            return {
                ...state,
                userPrefsFetching:true
            }
        case constants.RESPONSE_USERPREFS:
            return {
                ...state,
                userPrefs:action.payload,
                userPrefsFetching:false,
                userPrefsFetched:true
            }
        case constants.START_SAVEUSERPREFS:
            return {
                ...state,
                userPrefsSaving:true
            }
        case constants.FINISH_SAVEUSERPREFS:
            return {
                ...state,
                userPrefsSaving:false,
                userPrefsSaved:true,
                userPrefs:action.payload
            }
        default:
            return state
    }
}

export function authenticationReducer(state={},action){
    switch(action.type){
        case constants.REQUEST_AUTHENTICATION:
            return {
                ...state,
                authenticationFetching:true
            }
        case constants.RESPONSE_AUTHENTICATION:
            return {
                ...state,
                authentication:action.payload,
                authenticationFetching:false,
                authenticationFetched:true
            }
        case constants.START_SAVEAUTHENTICATION:
            return {
                ...state,
                authenticationSaving:true
            }
        case constants.FINISH_SAVEAUTHENTICATION:
            return {
                ...state,
                authenticationSaving:false,
                authenticationSaved:true,
                authentication:action.payload
            }
        default:
            return state
    }
}
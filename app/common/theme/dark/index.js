'use strict'

export const constants = {
    loadingColor:"#FFFFFF",
    loadMoreColor:"#999999",
    tabBarItemColor:"#FFFFFF",
    tabBarSelectedItemColor:"dodgerblue"
}

export const htmlStyles = {
    p:{
        color:"#999",fontSize:14
    },
    h1:{color:"#999",fontSize:14},
    h2:{color:"#999",fontSize:14},
    h3:{color:"#999",fontSize:14},
    h4:{color:"#999",fontSize:14},
    h5:{color:"#999",fontSize:14},
    h6:{color:"#999",fontSize:14},
    li:{color:"#999",fontSize:14}
}

export const styles = {
    header:{
        backgroundColor:"#444",
        borderBottomColor:"#000"
    },
    loading:{
        color:"#FFFFFF"
    },
    container:{
        backgroundColor:"#333"
    }
}
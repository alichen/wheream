'use strict'
import * as constants from "./constant"

function requestUserPrefs(){
    return {
        type:constants.REQUEST_USERPREFS
    }
}

function responseUserPrefs(payload){
    return {
        type:constants.RESPONSE_USERPREFS,
        respondAt:Date.now(),
        payload
    }
}

export function fetchUserPrefs(param){
    return dispatch=>{
        dispatch(requestUserPrefs())
        global.storage.getItem("userPrefs").then(ret=>{
            dispatch(responseUserPrefs(ret))
        })
    }
}

function startSaveUserPrefs(){
    return {
        type:constants.START_SAVEUSERPREFS
    }
}

function finishSaveUserPrefs(payload){
    return {
        type:constants.FINISH_SAVEUSERPREFS,
        finishAt:Date.now(),
        payload
    }
}

export function saveUserPrefs(userPrefs){
    return dispatch=>{
        dispatch(startSaveUserPrefs())
        global.storage.setItem("userPrefs",userPrefs).then(ret=>{
            dispatch(finishSaveUserPrefs(userPrefs))
        })
    }
}

function requestAuthentication() {
    return {
        type:constants.REQUEST_AUTHENTICATION
    }
}

function responseAuthentication(payload){
    return {
        type:constants.RESPONSE_AUTHENTICATION,
        payload,
        respondAt:Date.now
    }
}

export function fetchAuthentication(){
    return dispatch=>{
        dispatch(requestAuthentication())
        global.storage.getItem("user").then(ret=>{
            dispatch(responseAuthentication(ret))
        })
    }
}

function startSaveAuthentication() {
    return {
        type:constants.START_SAVEAUTHENTICATION
    }
}

function finishSaveAuthentication(payload){
    return {
        type:constants.FINISH_SAVEAUTHENTICATION,
        payload,
        finishAt:Date.now
    }
}

export function saveAuthentication(user){
    return dispatch=>{
        dispatch(startSaveAuthentication())
        global.storage.setItem("user",user).then(ret=>{
            dispatch(finishSaveAuthentication(user))
        })
    }
}